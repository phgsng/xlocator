use anyhow::{anyhow, Result};
use clap::{Parser, Subcommand};
use log::{debug, error, info, trace};
use serde_derive::{Deserialize, Serialize};
use xrandr::{Monitor, XHandle};

use std::{convert::TryFrom,
          fmt,
          fs::File,
          hash::Hash,
          io::{BufWriter, Read, Write}};

fn main() -> Result<()>
{
    let job = Job::new()?;

    job.exec()
}

#[derive(Parser, Debug)]
struct Argv
{
    #[command(subcommand)]
    subcmd: Job,

    #[arg(short, long)]
    verbose: bool,
}

#[derive(Subcommand, Debug)]
enum Job
{
    /** Create matchable entry from current screen profile. */
    New
    {
        /** The profile will be referred to by this name; profile
        will be written to stdout if omitted. */
        #[arg(short, long)]
        name: Option<String>,
    },
    /** Match current screen profile against known profile. */
    Match,
    /** Print a defined profile. */
    Show
    {
        /** Name of the profile to print; if absent, lists all known profile. */
        #[arg(short, long)]
        name: Option<String>,
        /** Path to drop-in directory where to look for profile definitions. */
        #[arg(short, long, default_value = "~/.config/xlocator/profiles.d")]
        dir:  String,
    },
}

impl Job
{
    fn new() -> Result<Self>
    {
        let argv = Argv::parse();

        stderrlog::new()
            .verbosity(if argv.verbose { 4 } else { 2 })
            .module(module_path!())
            .init()?;

        Ok(argv.subcmd)
    }

    fn exec(self) -> Result<()>
    {
        match self {
            Self::New { name } => cmd_new(name),
            Self::Match => todo!("manana"),
            Self::Show { name, dir } => cmd_show(name, dir),
        }
    }
}

#[derive(Deserialize, Serialize, Hash)]
struct Screen
{
    name:  String,
    edids: Vec<String>,
    wd:    usize,
    ht:    usize,
}

impl fmt::Display for Screen
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        write!(
            f,
            "[{}] {}x{} edids=[{}]",
            self.name,
            self.wd,
            self.ht,
            self.edids.join(", "),
        )
    }
}

impl TryFrom<Monitor> for Screen
{
    type Error = anyhow::Error;

    fn try_from(mon: Monitor) -> Result<Self, Self::Error>
    {
        use xrandr::{Output, Property, PropertyValue};

        let Monitor { width_px, height_px, name, outputs, .. } = mon;

        assert!(width_px > 0);
        assert!(height_px > 0);

        let wd = width_px as usize;
        let ht = height_px as usize;

        let edids = outputs
            .into_iter()
            .map(|Output { name, properties, .. }| {
                let prop = properties.get("EDID").ok_or_else(|| {
                    anyhow!(
                        "Profile: monitor {} unusable, missing EDID property",
                        name
                    )
                })?;
                match prop {
                    Property { value: PropertyValue::Edid(e), .. } =>
                        Ok(base64::encode(e)),
                    junk =>
                        Err(anyhow!(
                            "EDIT property of monitor {} doesn’t contain an \
                             Edid value; have fun debugging this: {:?}",
                            &name,
                            junk
                        )),
                }
            })
            .collect::<Result<Vec<_>>>()?;
        Ok(Self { name, edids, wd, ht })
    }
}

#[derive(Deserialize, Serialize)]
struct Profile
{
    hostname: String,
    hash:     u64,
    #[serde(rename = "screen")]
    screens:  Vec<Screen>,
}

impl fmt::Display for Profile
{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result
    {
        let n = self.screens.len();
        writeln!(
            f,
            ">>> host {}, hash {}, screens: {}",
            self.hostname, self.hash, n
        )?;
        for (i, scr) in self.screens.iter().enumerate() {
            writeln!(f, "    {}/{} > {}", i + 1, n, scr)?;
        }

        Ok(())
    }
}

impl Profile
{
    fn new() -> Result<Self>
    {
        let hostname = get_hostname()?;

        let mut xh = XHandle::open()
            .map_err(|e| anyhow!("new: Error creating X11 handle: {}", e))?;

        let screens = xh
            .monitors()
            .map_err(|e| anyhow!("new: Error accessing X11 monitors: {}", e))?
            .into_iter()
            .map(|mon: Monitor| Screen::try_from(mon))
            .collect::<Result<Vec<Screen>>>()?;

        let hash = {
            use std::hash::Hasher;

            let mut hsh = std::collections::hash_map::DefaultHasher::new();
            screens.hash(&mut hsh);
            hostname.hash(&mut hsh);
            hsh.finish()
        };

        Ok(Self { hostname, hash, screens })
    }

    fn output(&self, out: &mut dyn Write) -> Result<()>
    {
        let toml = toml::to_string(self)?;
        out.write_all(toml.as_bytes())?;
        Ok(())
    }
}

fn try_open(path: &str) -> Result<File>
{
    std::fs::OpenOptions::new()
        .read(true)
        .open(path)
        .map_err(|e| anyhow!("error opening [{}] for reading: {}", path, e))
}

fn cmd_show(name: Option<String>, dir: String) -> Result<()>
{
    eprintln!("»»»» {:?}", name);
    eprintln!("»»»» {}", dir);

    if let Some(name) = name {
        show_profile(name, dir)
    } else {
        show_dropin_dir(dir)
    }
}

fn show_profile(name: String, dir: String) -> Result<()>
{
    let mut fh = None;

    let fname = if name.ends_with(".toml") {
        debug!("show_profile: .toml suffix of input name matches, using as-is");
        name
    } else {
        debug!("show_profile: extending input name with .toml suffix");
        format!("{}.toml", name)
    };

    if let Ok(f) = try_open(&fname) {
        debug!("show_profile: successfully opened input {} directly", fname);
        fh = Some(f);
    } else if let Ok(f) = try_open(&format!("{}/{}", dir, fname)) {
        debug!(
            "show_profile: successfully opened input {} in drop-in directory",
            dir
        );
        fh = Some(f);
    }

    let mut fh = fh.ok_or_else(|| {
        anyhow!(
            "show: no such file [{}] found locally or in drop-in dir [{}]",
            fname,
            dir
        )
    })?;

    let buf = {
        let mut buf = Vec::new();
        let n = fh.read_to_end(&mut buf)?;
        if n == 0 {
            return Err(anyhow!("file {} found empty", fname));
        } else {
            debug!("read {} B of toml data from {}", n, fname);
        }

        String::from_utf8(buf).map_err(|e| {
            anyhow!("failed to parse profile {} parse as UTF-8: {}", fname, e)
        })?
    };

    let prof: Profile = toml::from_str(&buf)?;

    println!("{}", prof);
    Ok(())
}

fn show_dropin_dir(dir: String) -> Result<()> { todo!("manana") }

fn cmd_new(name: Option<String>) -> Result<()>
{
    debug!("creating new profile");

    let prof = Profile::new()?;

    if let Some(name) = name {
        let fname = if name.ends_with(".toml") {
            name
        } else {
            format!("{}.toml", name)
        };
        let fh = std::fs::OpenOptions::new()
            .create(true)
            .write(true)
            .open(&fname)?;
        let mut wr = BufWriter::new(fh);
        prof.output(&mut wr)?;
        debug!("success. Wrote profile to {}.", fname);
    } else {
        let stdout = std::io::stdout().lock();
        prof.output(&mut BufWriter::new(stdout))?;
        debug!("success. Wrote profile to stdout.");
    }

    Ok(())
}

/** Obtain the DNS name of the system we’re running on, preferably from
the canonical location at ``/etc/hostname``. */
fn get_hostname() -> Result<String>
{
    let mut fh =
        std::fs::OpenOptions::new().read(true).open("/etc/hostname")?;
    let mut buf = Vec::new();
    match fh.read_to_end(&mut buf)? {
        n if n == 0 => return Err(anyhow!("/etc/hostname found empty")),
        n => debug!("read hostname of {} B from /etc/hostname", n),
    }
    String::from_utf8(buf).map(|s| s.trim().to_string()).map_err(|e| {
        anyhow!(
            "lol, hostname from /etc/hostname doesn’t parse as UTF-8: {}",
            e
        )
    })
}
